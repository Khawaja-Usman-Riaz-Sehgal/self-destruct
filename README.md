# Self Destruct Initialized

- This is a Self Destruct code of a system which activates after 
  someone enters invalid access code for more than three times
- In order to use this code you need to clone the git repository first
- To clone a repository find the blue color button (Code) click it.
- You will find a URL in Clone with HTTPS. Copy it and open your Linux terminal
- write `git clone <insert URL>` to Clone the Repository
- After the cloning run the code using `g++ -o selfdestruct self-destruct-improved.cpp`
- To make sure the output is correct use `./selfdestruct`
- Happy Coding!
