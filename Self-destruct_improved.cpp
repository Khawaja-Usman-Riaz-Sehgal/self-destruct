#include <iostream>
#include<time.h>
using namespace std;
void delay(int delay) 
{ 
 int now=time(NULL); 
 int later=now+delay; 
 while(now<=later)now=time(NULL); 
} 
int main()
{
    int access_code,tries=3;
    cout<<"Please enter the access codes to enter"<<endl;
    cin>>access_code;
    int code=12345;
    while (tries!=0)
    {
        if(access_code!=code)
        {
            cout<<"You have entered wrong access code \n";
            cout<<"Please enter the right access code \n";
            cout<<"Please enter the access codes to enter \n";
            cin>>access_code; 
            cout<<"You have try ";
            tries=tries-1;
            cout<<tries;
            cout<<" more times \n";
        }
        else
        {
            cout<<"Welcome to our system. How may I assist you? \n";
            break;
        }        

    }
    if(tries==0 && access_code !=code)
    {
    cout<<"The System's Self Destruct is activated \n";
    cout<<"The System is going to self Destruct in....\n";
    for (int i = 10; i >= 0 ; i--)
    {
        delay(0.5);
        cout<<i<<"\n";
    }
    cout<<"Boom!!!!"<<endl;
    }
    
    return 0;
}